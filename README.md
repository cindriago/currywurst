This app intention was to practice and adquire knowledge on the usage of:

Springboot
React + Redux

By emulating the functionality of a restaurant. The premises are the following:

The customer places an order. Order should be cooked by cook in a given time.
The Cook works on a kitchen. The shop can have multiply cooks, which are working parallel. 
If there are not enough ingredients on the kitchen, the cook can ask the required amount of them from the warehouse. 
If all cooks are busy, order should wait, until it will be a cook available.
The warehouse is managed by workers. It can be more than one worker in the warehouse. All of them are working parallel. 
If all workers are already busy, ingredients can not be delivered.
Cooking time of the order is a summary of time, required for cooking and delivery of all ingredients (including waiting time for free cooks and workers).
The order can be accepted, if it can be processed in given or smaller time.


The app is available at:
https://currywurst2335.herokuapp.com/

It can also be executed locally with Docker, or individually by starting the springboot application and the react application separately.
